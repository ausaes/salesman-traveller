<?php

use App\Domain\Service\GetOrderedCitiesByBestRoute;
use App\Infrastructure\Repository\FileCitiesRepository;

require __DIR__ . '/vendor/autoload.php';

$service = new GetOrderedCitiesByBestRoute(new FileCitiesRepository());
$cityList = $service->execute();
foreach ($cityList->cities() as $city) {
    echo $city->name() . "\n";
}
<?php

namespace App\Domain\Entity;

use App\Domain\ValueObject\Coordinates;

class City
{
    private string $name;
    private Coordinates $coordinates;

    public function __construct(string $name, Coordinates $coordinates)
    {
        $this->name = $name;
        $this->coordinates = $coordinates;
    }

    public function name(): string
    {
        return $this->name;
    }

    public function coordinates(): Coordinates
    {
        return $this->coordinates;
    }

    public function equals(self $city): bool
    {
        return $this->name === $city->name;
    }
    public function getDistanceBetween(City $city): int
    {
        return $this->coordinates()->getDistanceFrom($city->coordinates());
    }
}

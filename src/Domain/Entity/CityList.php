<?php

namespace App\Domain\Entity;


class CityList
{
    /** @var City[] */
    private array $cities;


    public function __construct(array $cities)
    {
        $this->cities = $cities;
    }

    public function add(City $city): CityList
    {
        $cities = $this->cities();
        $cities[] = $city;
        return new CityList($cities);
    }

    public function getClosestCity(City $mainCity): City
    {
        $closestCity = null;
        foreach ($this->cities as $city) {
            if ($mainCity->equals($city)) {
                continue;
            }
            if (is_null($closestCity)) {
                $closestCity = $city;
            }
            if ($mainCity->getDistanceBetween($city) < $mainCity->getDistanceBetween($closestCity)) {
                $closestCity = $city;
            }
        }
        return $closestCity;
    }

    public function cities(): array
    {
        return $this->cities;
    }

    public function extract(City $cityToExtract): CityList
    {
        $cities = $this->cities();
        foreach($cities as $index => $city) {
            if($city->equals($cityToExtract)) {
                unset($cities[$index]);
            }
        }
        return new CityList($cities);
    }

}

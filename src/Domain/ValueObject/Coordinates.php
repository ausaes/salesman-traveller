<?php

namespace App\Domain\ValueObject;

class Coordinates
{
    private float $latitude;
    private float $longitude;

    public function __construct(float $latitude, float $longitude)
    {
        $this->latitude = $latitude;
        $this->longitude = $longitude;
    }

    public function latitude(): float
    {
        return $this->latitude;
    }

    public function longitude(): float
    {
        return $this->longitude;
    }
    public function getDistanceFrom(self $coordinates): int
    {
        $latFrom = deg2rad($this->latitude());
        $lonFrom = deg2rad($this->longitude());
        $latTo = deg2rad($coordinates->latitude());
        $lonTo = deg2rad($coordinates->longitude());

        $latDelta = $latTo - $latFrom;
        $lonDelta = $lonTo - $lonFrom;

        $angle = 2 * asin(sqrt(pow(sin($latDelta / 2), 2) +
                cos($latFrom) * cos($latTo) * pow(sin($lonDelta / 2), 2)));
        return $angle * 6371000;
    }


}

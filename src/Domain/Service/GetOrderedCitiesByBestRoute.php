<?php

namespace App\Domain\Service;

use App\Domain\Entity\CityList;
use App\Domain\Repository\CitiesRepository;

class GetOrderedCitiesByBestRoute
{
    private CitiesRepository $repository;


    public function __construct(CitiesRepository $repository)
    {
        $this->repository = $repository;
    }

    public function execute()
    {
        return $this->orderCitiesByStartingCity();
    }

    private function orderCitiesByStartingCity(): CityList
    {
        $cityList = $this->repository->getCityList();
        $city = $cityList->cities()[0];
        $orderedCityList = new CityList([$city]);
        $cityList = $cityList->extract($city);

        while (!empty($cityList->cities())) {
            $city = $cityList->getClosestCity($city);
            $orderedCityList = $orderedCityList->add($city);
            $cityList = $cityList->extract($city);
        }
        return $orderedCityList;
    }
}

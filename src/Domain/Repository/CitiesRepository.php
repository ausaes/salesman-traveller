<?php

namespace App\Domain\Repository;

use App\Domain\Entity\CityList;

interface CitiesRepository
{
    public function getCityList(): CityList;
}

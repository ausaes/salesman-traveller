<?php

namespace App\Infrastructure\Repository;

use App\Domain\Entity\City;
use App\Domain\Entity\CityList;
use App\Domain\Repository\CitiesRepository;
use App\Domain\ValueObject\Coordinates;

class FileCitiesRepository implements CitiesRepository
{
    private const FILE_NAME = 'cities.txt';

    public function getCityList(): CityList
    {
        return $this->convertRawToCities(explode("\n", $this->getFile()));
    }

    private function getFile()
    {
        return file_get_contents(self::FILE_NAME);
    }

    /**
     * @param array $rawCities
     * @return City[]
     */
    private function convertRawToCities(array $rawCities): CityList
    {
        $cities = [];
        foreach ($rawCities as $rawCity) {
            $cities[] = $this->extractCity($rawCity);
        }
        return new CityList($cities);
    }

    private function extractCity($rawCity): City
    {
        $cityInfo = explode(' ', $rawCity);
        $longitude = array_pop($cityInfo);
        $latitude = array_pop($cityInfo);
        $name = implode(' ', $cityInfo);
        return new City($name, new Coordinates((float)$latitude, (float)$longitude));
    }
}

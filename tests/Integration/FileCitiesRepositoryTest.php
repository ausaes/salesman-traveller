<?php

namespace App\Tests\Integration;

use App\Domain\Entity\City;
use App\Domain\ValueObject\Coordinates;
use App\Infrastructure\Repository\FileCitiesRepository;
use PHPUnit\Framework\TestCase;

class FileCitiesRepositoryTest extends TestCase
{
    /** @test */
    public function itShouldReturnTheListOfCities()
    {
        $repository = new FileCitiesRepository();
        $cities = $repository->getCityList();
        $this->assertCount(32, $cities->cities());
        $this->assertEquals(new City('Beijing', new Coordinates(39.93, 116.4)), $cities->cities()[0]);
        $this->assertEquals('San Francisco', $cities->cities()[5]->name());
    }
}

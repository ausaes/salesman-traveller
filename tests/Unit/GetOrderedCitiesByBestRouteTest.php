<?php

namespace Unit;

use App\Domain\Entity\City;
use App\Domain\Entity\CityList;
use App\Domain\Repository\CitiesRepository;
use App\Domain\Service\GetOrderedCitiesByBestRoute;
use App\Domain\ValueObject\Coordinates;
use PHPUnit\Framework\TestCase;

class GetOrderedCitiesByBestRouteTest extends TestCase
{
    /** @test */
    public function itShouldReturnTheOrderedList()
    {
        $repository = $this->getMockBuilder(CitiesRepository::class)->getMock();
        $repository
            ->method('getCityList')
            ->willReturn($this->getCityList());
        $service = new GetOrderedCitiesByBestRoute($repository);
        $orderedList = $service->execute();
        $cities = $orderedList->cities();
        $this->assertCount(4, $cities);
        $this->assertEquals('A', $cities[0]->name());
        $this->assertEquals('B', $cities[1]->name());
        $this->assertEquals('D', $cities[2]->name());
        $this->assertEquals('C', $cities[3]->name());
    }

    private function getCityList(): CityList
    {
        return new CityList([
            new City('A', new Coordinates(1.0, 1.0)),
            new City('C', new Coordinates(10.0, -2.0)),
            new City('B', new Coordinates(1.0, 2.0)),
            new City('D', new Coordinates(0.0, -1.0))
        ]);
    }
}

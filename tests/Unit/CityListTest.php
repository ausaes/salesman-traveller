<?php

namespace Unit;

use App\Domain\Entity\CityList;
use App\Domain\Entity\City;
use App\Domain\ValueObject\Coordinates;
use PHPUnit\Framework\TestCase;

class CityListTest extends TestCase
{
    /** @test */
    public function givenAValidListThenItReturnsTheValidList()
    {
        $list = $this->getCityList();
        $this->assertCount(4, $list->cities());
    }

    /** @test */
    public function givenAValidListAndMainCityThenItReturnsTheClosestCity()
    {
        $list = $this->getCityList();
        $mainCity = new City('A', new Coordinates(1.0, 1.0));
        $closestCity = $list->getClosestCity($mainCity);
        $this->assertEquals(new City('B', new Coordinates(1.0, 2.0)), $closestCity);
    }

    private function getCityList(): CityList
    {
        return new CityList([
            new City('A', new Coordinates(1.0, 1.0)),
            new City('C', new Coordinates(10.0, -2.0)),
            new City('B', new Coordinates(1.0, 2.0)),
            new City('D', new Coordinates(0.0, -1.0))
        ]);
    }
}

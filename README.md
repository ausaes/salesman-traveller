# Travelling salesman problem.
This code uses the nearest neighbour (NN) algorithm to solve the problem.
Is based on PHP 7.4

## Build Project
```
docker-compose up
```

### Install dependencies
```
docker exec -it salesman-traveller.php composer install
```

### Run tests
```
docker exec -it salesman-traveller.php php vendor/bin/phpunit
```

### Execute the solution
```
docker exec -it salesman-traveller.php php solve.php
```